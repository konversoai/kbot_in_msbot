const { ComponentDialog, TextPrompt, WaterfallDialog } = require('botbuilder-dialogs');

const axios = require('axios').default;

const utils = require('util');

const TurndownService = require('turndown');


const setTimeoutPromisified = utils.promisify(setTimeout);

const REPEATABLE_DIALOG = 'REPEATABLE_DIALOG';

const TEXT_PROMPT = 'TEXT_PROMPT';

const WATERFALL_DIALOG = 'WATERFALL_DIALOG';

const turndownService = new TurndownService();

class KbotRepeatableDialog extends ComponentDialog {
    constructor() {
        super(REPEATABLE_DIALOG);

        // Set config defaults when creating the instance
        this.axiosInstance = axios.create({
            baseURL: `${process.env.KbotAPIHostName}`
        });

        this.token = undefined;
        this.conversationId = undefined;

        this.addDialog(new TextPrompt(TEXT_PROMPT));

        this.addDialog(new WaterfallDialog(WATERFALL_DIALOG, [
            this.waitingStep.bind(this),
            this.loopStep.bind(this)
        ]));

        this.initialDialogId = WATERFALL_DIALOG;
    }

    async waitingStep(stepContext) {
        const options = stepContext.options || {};

        let stopTopic = false;
        let wait = false;
        if (options.messages) {

            const hasStop = options.messages.find(message => message.type === 'stop_topic');
            if (hasStop) {
                stopTopic = true;
            }
            const hasWait = options.messages.find(message => message.type === 'wait_user_input');
            if (hasWait) {
                wait = true;
            }

            const messages = options.messages.filter(message => !['wait_user_input', 'stop_topic', 'typing'].includes(message.type));

            let messagesToPrompt = [];
            if (wait) {
                messagesToPrompt = messages.splice(messages.length - 1, 1);
            }
            messages.forEach(async (message) => {
                await stepContext.context.sendActivity(render(message.message));
            });

            if (stopTopic) {
                return await stepContext.endDialog();
            }

            if (wait) {
                const promptOptions = {
                    prompt: messagesToPrompt.reduce((text, message) => {
                        text += render(message.message);
                        return text;
                    }, '')
                };
                // Ask the user to enter their name.
                return await stepContext.prompt(TEXT_PROMPT, promptOptions);
            }
        } else {
            return stepContext.next();
        }
    }

    async loopStep(stepContext) {
        if (!this.token) {
            const token = await this.getKbotToken();
            if (!token) {
                await stepContext.context.sendActivity('Cannot authenticate to Kbot.');
                return await stepContext.endDialog();
            }
            this.token = token;
        }
        if (!this.token) {
            await stepContext.context.sendActivity('Cannot authenticate to Kbot.');
            return await stepContext.endDialog();
        }

        // Alter defaults after instance has been created
        this.axiosInstance.defaults.headers.common['Authorization'] = this.token;

        if (!this.conversationId) {
            const conversationId = await this.createKbotConversation();
            if (conversationId) {
                this.conversationId = conversationId;
            }
        }

        if (!this.conversationId) {
            await stepContext.context.sendActivity('Cannot create conversation with Kbot.');
            return await stepContext.endDialog();
        }

        const response = await this.messageToKbotConversation(this.conversationId, stepContext.context.activity.text);
        if (!response) {
            await stepContext.context.sendActivity('Cannot send message to Kbot.');
            return await stepContext.endDialog();
        }

        if (!this.conversationId) {
            await stepContext.context.sendActivity('Cannot create conversation with Kbot.');
            return await stepContext.endDialog();
        }

        const messages = await this.readKbotConversationAnswers(this.conversationId);

        return await stepContext.replaceDialog(REPEATABLE_DIALOG, { messages: messages });
    }

    async getKbotToken() {
        return this.axiosInstance.post('/api/login/', {
            external_system: 'plain_login_pass',
            login_pass: `${process.env.KbotLogin}`,
            kbot_application_id: 'kbot_in_msbot'
        }).then((response) => {
            const data = response.data;
            if (data.access_token) {
                return data.access_token;
            } else {
                throw new Error('No token got');
            }
        })
            .catch((error) => {
                console.log('Error get token', error);

                return null;
            });
    }

    async createKbotConversation() {
        return this.axiosInstance.post('/api/conversation/', {
            username: 'bot'
        })
            .then((response) => {
                const data = response.data;
                if (data.id) {
                    return data.id;
                } else {
                    throw new Error('Conversation is not created');
                }
            })
            .catch((error) => {
                console.log('Error create conversation', error);

                return null;
            });
    }

    async messageToKbotConversation(conversationId, message) {
        const sendData = {
            ...this.createMessage(message),
            conversation_id: conversationId
        };
        return this.axiosInstance.post(`/api/conversation/${conversationId}/message/`, sendData)
            .then((response) => {
                const data = response.data;
                return data;
            })
            .catch((error) => {
                console.log('Error send message', error);

                return null;
            });
    }

    async readKbotConversationAnswers(conversationId) {
        let messages = [];
        let stop = false;
        while (!stop) {
            const messagesGot = await this.readKbotConversation(conversationId);
            const messageIdx = messagesGot.findIndex(mess => ['wait_user_input', 'stop_topic'].includes(mess.type));
            if (messageIdx >= 0) {
                stop = true;
            }
            messages = messages.concat(messagesGot);

            if (!stop) {
                await setTimeoutPromisified(2000);
            }
        }

        return messages;
    }

    async readKbotConversation(conversationId) {
        return this.axiosInstance.get(`/api/conversation/${conversationId}/`)
            .then((response) => {
                const data = response.data;
                return data;
            })
            .catch((error) => {
                console.log('Error read messages', error);
                return [];
            });
    }

    createMessage(messageText) {
        return {
            type: 'message',
            message: messageText,
            isSending: true,
            fromUser: true
        };
    }
}

function render(messages) {
    if (Array.isArray(messages)) {
        return messages.filter((message) => message.format !== 'hint')
            .reduce((accum, message) => {
                try {
                    // console.log(JSON.stringify(message, null, '    '));
                    let ordered = false;
                    let counter = 1;
                    switch (message.format) {
                        case 'text':
                            accum += turndownService.turndown(message.value) + '\n';
                            break;

                        case 'html':
                            accum += turndownService.turndown(message.value) + '\n';
                            break;
                        case 'block':
                            accum += render(message.value);
                            break;
                        case 'SearchResults':
                            if (Array.isArray(message.value)) {
                                message.value.forEach((value, index) => {
                                    let title = value.title;
                                    if (value.data && value.data.url) {
                                        title = `[${title}](${value.data.url})`;
                                    }
                                    accum += `${index + 1}. ## ${title} \n`;
                                    if (value.snippet && Array.isArray(value.snippet)) {
                                        accum += '    ' + render(value.snippet) + '\n';
                                    }
                                });
                            }
                            break;
                        case 'list':
                            if (message.data && message.data.type && message.data.type === 'ordered') {
                                ordered = true;
                            }
                            message.value.forEach(value => {
                                let child = render(value);
                                if (child) {
                                    if (ordered) {
                                        child = `${counter}. ${child}`;
                                        counter++;
                                    } else {
                                        child = `* ${child}`;
                                    }
                                    accum += child + '\n';
                                }
                            });

                            break;
                        default:
                            accum += JSON.stringify(message.value);
                            break;
                    }
                } catch (error) {
                    console.log('got error during render', error);
                }
                return accum;
            }, '');
    } else {
        console.log('got error during render', error);
        return '';
    }
}

module.exports.KbotRepeatableDialog = KbotRepeatableDialog;
module.exports.REPEATABLE_DIALOG = REPEATABLE_DIALOG;
