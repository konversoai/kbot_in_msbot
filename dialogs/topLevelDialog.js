const { ComponentDialog, WaterfallDialog } = require('botbuilder-dialogs');

const { LuisRecognizer, QnAMaker } = require('botbuilder-ai');

const { KbotRepeatableDialog, REPEATABLE_DIALOG } = require('./kbotRepeatableDialog');

const TOP_LEVEL_DIALOG = 'TOP_LEVEL_DIALOG';

const WATERFALL_DIALOG = 'WATERFALL_DIALOG';

class TopLevelDialog extends ComponentDialog {
    constructor() {
        super(TOP_LEVEL_DIALOG);

        // If the includeApiResults parameter is set to true, as shown below, the full response
        // from the LUIS api will be made available in the properties  of the RecognizerResult
        this.dispatchRecognizer = new LuisRecognizer({
            applicationId: process.env.LuisAppId,
            endpointKey: process.env.LuisAPIKey,
            endpoint: `https://${process.env.LuisAPIHostName}.api.cognitive.microsoft.com`
        }, {
            includeAllIntents: true,
            includeInstanceData: true
        }, true);

        this.qnaHRMaker = new QnAMaker({
            knowledgeBaseId: process.env.QnAHRKnowledgebaseId,
            endpointKey: process.env.QnAHREndpointKey,
            host: process.env.QnAHREndpointHostName
        });

        this.qnaHealthInsuranceMaker = new QnAMaker({
            knowledgeBaseId: process.env.QnAHealthInsuranceKnowledgebaseId,
            endpointKey: process.env.QnAHealthInsuranceEndpointKey,
            host: process.env.QnAHealthInsuranceEndpointHostName
        });

        this.addDialog(new KbotRepeatableDialog());

        this.addDialog(new WaterfallDialog(WATERFALL_DIALOG, [
            this.initialStep.bind(this)
        ]));

        this.initialDialogId = WATERFALL_DIALOG;
    }

    async initialStep(stepContext) {
        // console.log('Top level initial step', stepContext);
        // First, we use the dispatch model to determine which cognitive service (LUIS or QnA) to use.
        const recognizerResult = await this.dispatchRecognizer.recognize(stepContext.context);

        // Top intent tell us which cognitive service to use.
        const intent = LuisRecognizer.topIntent(recognizerResult);

        // Next, we call the dispatcher with the top intent.
        return await this.dispatchToTopIntentAsync(stepContext, intent, recognizerResult);
    }

    async dispatchToTopIntentAsync(stepContext, intent, recognizerResult) {
        switch (intent) {
            case 'l_Weather':
                return await this.processWeather(stepContext, recognizerResult.luisResult);
            case 'l_Kbot':
                return await this.processWithKbot(stepContext);
            case 'q_hr':
                return await this.processQnAHR(stepContext);
            case 'q_health_insurance':
                return await this.processQnAHealthInsurance(stepContext);
            default:
                console.log(`Dispatch unrecognized intent: ${intent}.`);
                await stepContext.context.sendActivity(`Dispatch unrecognized intent: ${intent}.`);
                return await stepContext.endDialog();
        }
    }

    async processWeather(stepContext, luisResult) {
        console.log('processWeather');

        // Retrieve LUIS results for Weather.
        const result = luisResult.connectedServiceResult;
        const topIntent = result.topScoringIntent.intent;

        await stepContext.context.sendActivity(`ProcessWeather top intent ${topIntent}.`);
        await stepContext.context.sendActivity(`ProcessWeather intents detected:  ${luisResult.intents.map((intentObj) => intentObj.intent).join('\n\n')}.`);

        if (luisResult.entities.length > 0) {
            await stepContext.context.sendActivity(`ProcessWeather entities were found in the message: ${luisResult.entities.map((entityObj) => entityObj.entity).join('\n\n')}.`);
        }
        return await stepContext.endDialog();
    }

    async processQnAHR(stepContext) {
        console.log('processQnAHR');

        const results = await this.processSampleQnA(this.qnaHRMaker, stepContext);

        if (results.length > 0) {
            await stepContext.context.sendActivity(`${results[0].answer}`);
        } else {
            await stepContext.context.sendActivity('Sorry, could not find an answer in the HR Q and A system.');
        }

        return await stepContext.endDialog();
    }

    async processQnAHealthInsurance(stepContext) {
        console.log('processQnAHealthInsurance');

        const results = await this.processSampleQnA(this.qnaHealthInsuranceMaker, stepContext);

        if (results.length > 0) {
            await stepContext.context.sendActivity(`${results[0].answer}`);
        } else {
            await stepContext.context.sendActivity('Sorry, could not find an answer in the Health Insurance Q and A system.');
        }

        return await stepContext.endDialog();
    }

    async processSampleQnA(qnaMaker, stepContext) {
        const results = await qnaMaker.getAnswers(stepContext.context);

        return results || [];
    }

    async processWithKbot(stepContext) {
        console.log('processWithKbot');
        await stepContext.context.sendActivity('Switching to Kbot!');
        return await stepContext.beginDialog(REPEATABLE_DIALOG);
    }
}

module.exports.TopLevelDialog = TopLevelDialog;
module.exports.TOP_LEVEL_DIALOG = TOP_LEVEL_DIALOG;
