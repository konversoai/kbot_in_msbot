# Bot Builder v4 example with Kbot integration

## Pre-requisites:
To try the example you need:

- [Node.js](https://nodejs.org/) version 10.14 or higher
- [dotnetcore 2.1](https://www.microsoft.com/net/download/dotnet-core/2.1) for `luisgen` and `botdispatch`

## To try this sample

- Clone the repository
- In a terminal, navigate to `kbot_in_msbot`

    ```
    cd bot-builder-kbot
    ```

- `luisgen` is a peer dependency, so you need to install it globally with:
    
    ```
    npm install -g luisgen
    ```

- Install modules

    ```
    npm install
    ```

- Update environment configuration to change LUIS, QnA Maker and Kbot credentials
    Edit `.env` file in `kbot_in_msbot` folder. Change `KbotAPIHostName` parameter to point to your instance of Kbot
   

- Start the bot

    ```
    npm start
    ```

## Testing the bot using Bot Framework Emulator

[Bot Framework Emulator](https://github.com/microsoft/botframework-emulator) is a desktop application that allows bot developers to test and debug their bots on localhost or running remotely through a tunnel.

- Install the Bot Framework Emulator version 4.3.0 or greater from [here](https://github.com/Microsoft/BotFramework-Emulator/releases)

### Connect to the bot using Bot Framework Emulator

- Launch Bot Framework Emulator
- File -> Open Bot
- Enter a Bot URL of `http://localhost:3978/api/messages`

